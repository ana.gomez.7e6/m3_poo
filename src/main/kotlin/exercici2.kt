//Programa que rep el nom d'un color i diu si aquest está incorporat en l'arc de St Martí


enum class ColorsArc{
    vermell, taronja, groc, verd, blau, cian, violeta
}

fun conteColor(colorUsuari:String): Boolean {
    return try {
        ColorsArc.valueOf(colorUsuari)
        true
    }catch (e:Exception){
        false
    }
}

fun main() {
    println("Introdueix un color:")
    val colorUsuari = readln()
    println(conteColor(colorUsuari))
}
