/*
Volem fer un petit programa d'emulació d'instruments.
Tenim els següents instruments: Tambor, Triangle
Cada tambor té un to que pot ser A, O, U.
Els tambors amb A fan TAAAM, els tambors amb O fan TOOOM i els tambors amb U fan TUUUM.
Els triangles tenen una resonancia que va del 1 al 5.
Els triangles amb resonancia 1 fan TINC, els de dos TIINC, … i els de 5 TIIIIINC.
*/

open class Instrument{
    open fun makeSounds(times: Int){}
}
class Triangle(_resonancia:Int): Instrument() {
    val resonancia = _resonancia
    override fun makeSounds(times: Int){
        var so = "T"
        repeat(resonancia){
            so+="I"
        }
        so+="NC"
        repeat(times){println(so)}
    }
}

class Drump(_to: String): Instrument() {
    val to = _to
    override fun makeSounds(times: Int) {
        var so="T"
        repeat(3){
            so+=to
        }
        so+="M"
        repeat(times){println(so)}
    }
}

fun main() {
    val instruments: List<Instrument> = listOf(
        Triangle(5),
        Drump("A"),
        Drump("O"),
        Triangle(1),
        Triangle(5)
    )
    play(instruments)
}

private fun play(instruments: List<Instrument>) {
    for (instrument in instruments) {
        instrument.makeSounds(2) // plays 2 times the sound
    }
}