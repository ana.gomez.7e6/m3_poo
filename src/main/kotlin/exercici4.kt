/*
Volem fer un petit programa de preguntes.
Tenim dos tipus de preguntes:
 -opció múltiple
 -text lliure

Necessitaràs les següents classes
 -Question
 -FreeTextQuestion
 -MultipleChoiseQuestion
 -Quiz (conté una llista de preguntes)

Pas 1: Fes l'estructura de dades que et permeti guardar aquestes preguntes.
Pas 2: Fes un programa que, donat un quiz creat per codi, imprimeixi les preguntes.
Pas 3: Permet que l'usuari pugui respondre les preguntes.
Pas 4: Un cop acabat, imprimeix quantes respostes són correctes.
*/
//Classe principal
open class Question(){
    open fun imprimir():Boolean{return true}
}
//Classe d'opció lliure
class FreeTextQuestion(_pregunta:String, respuesta:String): Question() {
    val pregunta = _pregunta
    val resCorrecta = respuesta
    override fun imprimir():Boolean{
        println(pregunta)
        var respuesta = readln()
        return respuesta==resCorrecta
    }

    fun responder(){

    }
}
//Classe d'opció múltiple
class MultipleChoiceQuestion(_pregunta: String, _opciones:List<String>, respuesta: Int): Question() {
    val pregunta = _pregunta
    val opciones = _opciones
    val resCorrecta= respuesta
    override fun imprimir():Boolean{
        println("$pregunta")
        for(i in 0 until opciones.size){
            println("$i - ${opciones[i]}")
        }

        var respuesta = readln().toInt()
        return respuesta==resCorrecta
    }
}

class Quiz(lista : List<Question>){
    var lista=lista
    var contador =0

    fun imprimir(){

        for (pregunta in lista){
            var correcto =pregunta.imprimir()
            println()

            if(correcto) contador++
        }

        println("Respostes correctes = $contador")
    }
}

fun main() {

    val pregunta = FreeTextQuestion("Oro parece, plata no es", "platano")
    val pregunta2 = MultipleChoiceQuestion("Oro parece, plata no es", listOf("platano", "oro","plata"),0)
    val lista = listOf(pregunta, pregunta2)
    val var_quiz = Quiz(lista)
    var_quiz.imprimir()
}