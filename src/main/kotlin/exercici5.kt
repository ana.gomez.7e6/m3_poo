/*Volem fer un programa pel control d'entrada d'un club esportiu. El programa ha de ser generic per tots els clubs,
però tenim clubs que utilitzen un dispositiu de lectura dactilar, uns altres fan servir una targeta electrònica, uns altres enregistren l'entrada manualment…
Tots els gimnasos tenen un únic punt d'entrada i per tant, el primer cop que es registra un identificador indica que l'usuari ha entrat, i el segón cop indica
que l'usuari ha sortit.
Per tal de resoldre-ho, hem pactat una interfcície comuna anomenada GymControlReader que ens retorna l'identificador del següent usuari registrat:
fun nextId() : String
A més a més, ens donen la implementació següent que és la utilitzada pel sistema de lectura manual
class GymControlManualReader(val scanner:Scanner = Scanner(System.`in`)) : GymControlReader {
    override fun nextId() = scanner.next()
}
Crea un programa que, donat un GymControlReader, llegeixi 8 identificadors. Imprimeix per pantalla si l'usuari ha entrat o ha sortit. Utilitza la implementació donada.
*/

/*class GymControlManualReader(val scanner: Scanner = Scanner(System.`in`)) : GymControlReader {
    override fun nextId() = scanner.next()
}*/
fun main() {

}