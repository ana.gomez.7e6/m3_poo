/*
Volem enmagatzemar d'un estudiant, el seu nom, i la seva nota en format text.
Una nota pot ser suspès, aprovat, bé, notable o excel·lent.
Fes un programa que crei 2 estudiants amb dues notes diferents i printa'ls per pantalla
*/

class Evaluacio(
    val nom : String,
    val nota : String
)
fun main() {
    val mar = Evaluacio("Mar","Suspes")
    val joan = Evaluacio("Joan","Aprovat")

    println("""
        EVALUACIÓ
        ${mar.nom}, ${mar.nota}
        ${joan.nom}, ${joan.nota}
    """.trimIndent())
}